﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyArrayList
{
    interface IAList
    {
        public void Clear(); 
        public int Size();
        public int Get(int index); 
        public bool Add(int value); 
        public bool Add(int index, int value); 
        public int Remove(int value); 
        public int RemoveByIndex(int index); 
        public bool Contains(int value); 
        public bool Set(int index, int value);
        public void Print(); 
        public int[] ToArray();
        public bool RemoveAll(int[] arr);
    }
}
