﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyArrayList
{
    public class myArrayList : IAList
    {
        private const int DEFAULT_CAPACITY = 10;
        private int[] array;
        private int size;
        public myArrayList()
        {
            array = new int[DEFAULT_CAPACITY];
        }
        public myArrayList(int value)
        {
            if (value < DEFAULT_CAPACITY)
                array = new int[DEFAULT_CAPACITY];
            else
                array = new int[value];
        }
        public myArrayList(int[] _array)
        {
            if (_array.Length < DEFAULT_CAPACITY)
            {
                array = new int[DEFAULT_CAPACITY];
                for (int i = 0; i < _array.Length; i++)
                {
                    size++;
                    array[i] = _array[i];
                }
            }
            else
            {
                array = _array;
                size = array.Length - 1;
            }
        }
        public void Clear()
        {
            array = new int[DEFAULT_CAPACITY];
            size = 0;
        }
        public int Size() => size;
        public int Get(int index) => array[index];
        public bool Add(int value)
        {
            bool isAdd = false;
            if (size == array.Length)
            {
                int[] tempArray = new int[size * 2];

                for (int i = 0; i < array.Length; i++)
                    tempArray[i] = array[i];

                array = tempArray;
                array[size++] = value;
                isAdd = true;
            }
            else
            {
                array[size++] = value;
                isAdd = true;
            }
            return isAdd;
        }
        public bool Add(int index, int value)
        {
            bool isAdded = false;
            try
            {
                if (index <= size)
                {
                    int[] tempArr = new int[array.Length + 1];
                    Array.Copy(array, 0, tempArr, 0, index);
                    tempArr[index] = value;
                    Array.Copy(array, index, tempArr, index + 1, array.Length - index);
                    array = tempArr;
                    size++;
                    isAdded = true;
                }
                else
                {
                    throw new Exception("Your index is outside the array");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            return isAdded;
        }
        public int Remove(int value) 
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == value)
                {
                    int[] tempArr = new int[array.Length - 1]; 
                    Array.Copy(array, 0, tempArr, 0, i);
                    Array.Copy(array, i + 1, tempArr, i, tempArr.Length - i);
                    array = tempArr;
                    size--;
                    break;
                }
            }
            return value;
        }
        public int RemoveByIndex(int index) 
        {
            int value = 0;
            try
            {
                if (index < size)
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        if (i == index)
                        {
                            value = array[i];
                            int[] tempArr = new int[array.Length - 1]; //14 => 13
                            Array.Copy(array, 0, tempArr, 0, i);//
                            Array.Copy(array, i + 1, tempArr, i, tempArr.Length - i);
                            array = tempArr;                            
                            size--;
                            break;
                        }
                    }
                }
                else throw new Exception("Element is outside of myArrayList");
            }
            catch(Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            return value;
        }
        public bool Contains(int value)
        {
            bool isContains = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == value)
                    isContains = true;
            }
            return isContains;
        }
        public bool Set(int index, int value)
        {
            bool isSet = false;
            try
            {
                if(index < size)
                {
                    array[index] = value;
                    isSet = true;
                }
                else throw new Exception("Index element is outside of myArrayList");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            return isSet;
        }
        public void Print()
        {
            for (int i = 0; i < size; i++)
            {
                if(i == size - 1)
                    Console.Write(array[i]);
                else
                Console.Write(array[i] + ", ");
            }
            Console.WriteLine();
        }
        public int[] ToArray() => array;

        public bool RemoveAll(int[] arr)
        {
            bool isRemove = false;
            int[] tempArray = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (array[i] == arr[j])
                    {
                        Remove(array[i]);
                        isRemove = true;
                        i--;
                        break;
                    }                  
                }
            }
            return isRemove;
        }
    }
}
